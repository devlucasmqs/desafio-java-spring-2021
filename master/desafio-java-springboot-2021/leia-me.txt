###############################################
#  ------------     IMPORTANTE      -----------
#  CONFIGURAÇÕES UTILIZADAS
#  BANCO DE DADOS UTILIZADO MYSQL
#  FAVOR VERIFICAR AS CONFIGURAÇÕES DO MYSQL
###############################################

server.port=9999

spring.jpa.properties.hibernate.format_sql = true

spring.jackson.date-format=yyyy-MM-dd




#### MYSQL CONNECTION
spring.jpa.show-sql=true
spring.datasource.url=jdbc:mysql://localhost:3306/mysql?useTimezone=true&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=masterkey

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto=create-drop
