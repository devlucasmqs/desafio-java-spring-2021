package com.fb.springrest.api.uteis;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Random;

public class Util {

	public static int RandomRange(int AFrom, int ATo) {
		Random SRandom = new Random();
		return SRandom.nextInt((ATo - AFrom)) + AFrom;
	}

	public static String getStackTrace(Throwable aThrowable) {
		Writer SResult = new StringWriter();
		PrintWriter SPrintWriter = new PrintWriter(SResult);
		aThrowable.printStackTrace(SPrintWriter);
		return SResult.toString();
	}
	
	public static String CreateID() {
		String sID; 
		Calendar sCalendario = Calendar.getInstance();
		Random sRandom = new Random(sCalendario.get(Calendar.MILLISECOND));
		
		sID = "";
		sID = sID +	String.format("%04d", sCalendario.get(Calendar.YEAR));
		sID = sID + String.format("%02d", sCalendario.get(Calendar.MONTH) + 1);
		sID = sID + String.format("%02d", sCalendario.get(Calendar.DAY_OF_MONTH));
		sID = sID + String.format("%02d", sCalendario.get(Calendar.HOUR_OF_DAY));
		sID = sID + String.format("%02d", sCalendario.get(Calendar.MINUTE));
		sID = sID + String.format("%02d", sCalendario.get(Calendar.SECOND));
		sID = sID + String.format("%06d", sRandom.nextInt(999999));
		
		return sID;
	}
	
    public static String MD5(String texto) {
        MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
        md.update(texto.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", 0xFF & b));
        }
        return sb.toString();
    }

    public static String SHA256(String texto) {
        MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
        md.update(texto.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", 0xFF & b));
        }
        return sb.toString();
    }
    
    public static String LPad (String texto, int tamanho, String pad) {
	    while (texto.length() < tamanho) {
            texto = pad + texto;
        }
        return texto;
    }

    public static String RPad (String texto, int tamanho, String pad) {
        while (texto.length() < tamanho) {
            texto = texto + pad;
        }
        return texto;
    }
}
