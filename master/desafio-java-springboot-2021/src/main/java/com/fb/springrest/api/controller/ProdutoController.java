package com.fb.springrest.api.controller;

import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fb.springrest.api.uteis.Util;
import com.fb.springrest.api.exception.ProdutoNotFoundException;
import com.fb.springrest.api.model.Produto;
import com.fb.springrest.api.repository.IProdutoRepository;

@RestController
@RequestMapping("/products")
@CrossOrigin
public class ProdutoController {
	@Autowired
	private IProdutoRepository produtoRepository;
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<Produto> listar(){
		return produtoRepository.findAll();
	}
	
	@GetMapping(value = {"/{id}"})
	@ResponseStatus(code = HttpStatus.OK)
	public Optional<Produto> listar(@PathParam("id") String id){
		Optional<Produto> prod = produtoRepository.findById(id);
		if (!prod.isPresent()) {
			throw new ProdutoNotFoundException(id);
		}
		
		return prod;
	}

	@GetMapping(value = {"/search"})
	@ResponseStatus(code = HttpStatus.OK)
	public List<Produto> pesquisar(@RequestParam(required = false, name = "q") Optional<String> q,
			@RequestParam(required = false, name = "min_price") Optional<Double> min_price,
			@RequestParam(required = false, name = "max_price") Optional<Double> max_price){
		if (q.isPresent()) {
			if (min_price.isPresent() && max_price.isPresent()) {
				return produtoRepository.findByNameDescriptionMinMaxPrice(q.get(), min_price.get(), max_price.get());				
			}
			else if (min_price.isPresent()) {
				return produtoRepository.findByNameDescriptionMinPrice(q.get(), min_price.get());
			}
			else if (max_price.isPresent()) {
				return produtoRepository.findByNameDescriptionMaxPrice(q.get(), max_price.get());
			}
			else {
				return produtoRepository.findByNameDescription(q.get());
			}
		}
		else {
			if (min_price.isPresent() && max_price.isPresent()) {
				return produtoRepository.findByMinMaxPrice(min_price.get(), max_price.get());				
			}
			else if (min_price.isPresent()) {
				return produtoRepository.findByMinPrice(min_price.get());
			}
			else if (max_price.isPresent()) {
				return produtoRepository.findByMaxPrice(max_price.get());
			}
			else {
				return produtoRepository.findAll();
			}
		}
	}	
		
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Produto salvar(@RequestBody Produto produto) {
		try {
			produto.setId(Util.CreateID());
			return produtoRepository.save(produto);
	    } catch (Exception ex) {
	        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Solicitação Inválida", ex);
	    }
	}
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Produto atualizar(@RequestBody Produto produto) {
		Optional<Produto> prod = produtoRepository.findById(produto.getId());
		if (!prod.isPresent()) {
			throw new ProdutoNotFoundException(produto.getId());
		}
		
		return produtoRepository.save(produto);
	}
	
	@DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deletePost(@PathVariable String id) {
		Optional<Produto> prod = produtoRepository.findById(id);
		if (!prod.isPresent()) {
			throw new ProdutoNotFoundException(id);
		}
		else {
			produtoRepository.deleteById(id);
			return new ResponseEntity<>(id, HttpStatus.OK);	
		}       
    }
}
