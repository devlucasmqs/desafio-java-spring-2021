package com.fb.springrest.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fb.springrest.api.model.Produto;


public interface IProdutoRepository extends JpaRepository<Produto, String>{
	final String SELECT_BASE = "SELECT P.* FROM PRODUTO P ";
	final String MIN_PRICE = " P.PRICE >= :min_price ";
	final String MAX_PRICE = " P.PRICE <= :max_price ";
	final String NAME_DESCRIPTION = " P.NAME = :name_desc OR P.DESCRIPTION = :name_desc";
	
	
	final String SELECT_MIN_PRICE = SELECT_BASE + " WHERE " + MIN_PRICE;
	final String SELECT_MAX_PRICE = SELECT_BASE + " WHERE " + MAX_PRICE;
	final String SELECT_MIN_MAX_PRICE = SELECT_BASE + " WHERE " + MIN_PRICE + " AND " + MAX_PRICE;
	final String SELECT_NAME_DESCRIPTION = SELECT_BASE + " WHERE " + NAME_DESCRIPTION;
	
	final String SELECT_NAME_DESCRIPTION_MIN_PRICE = 
			SELECT_BASE + " WHERE (" + NAME_DESCRIPTION + " ) AND " + MIN_PRICE;
	final String SELECT_NAME_DESCRIPTION_MAX_PRICE =
			SELECT_BASE + " WHERE (" + NAME_DESCRIPTION + " ) AND " + MAX_PRICE;
	final String SELECT_NAME_DESCRIPTION_MIN_MAX_PRICE =
			SELECT_BASE + " WHERE (" + NAME_DESCRIPTION + " ) AND " + MIN_PRICE + " AND " + MAX_PRICE;
	
	@Query(value = SELECT_MIN_PRICE, nativeQuery = true)
	List<Produto> findByMinPrice(@Param("min_price") double min_price);
	
	@Query(value = SELECT_MAX_PRICE, nativeQuery = true)
	List<Produto> findByMaxPrice(@Param("max_price") double max_price);
	
	@Query(value = SELECT_MIN_MAX_PRICE, nativeQuery = true)
	List<Produto> findByMinMaxPrice(@Param("min_price")double min_price, @Param("max_price") double max_price);
	
	@Query(value = SELECT_NAME_DESCRIPTION, nativeQuery = true)
	List<Produto> findByNameDescription(@Param("name_desc") String name_desc);
	
	@Query(value = SELECT_NAME_DESCRIPTION_MIN_PRICE, nativeQuery = true)
	List<Produto> findByNameDescriptionMinPrice(@Param("name_desc") String name_desc, @Param("min_price") double min_price);
	
	@Query(value = SELECT_NAME_DESCRIPTION_MAX_PRICE, nativeQuery = true)
	List<Produto> findByNameDescriptionMaxPrice(@Param("name_desc") String name_desc, @Param("max_price") double max_price);
	
	@Query(value = SELECT_NAME_DESCRIPTION_MIN_MAX_PRICE, nativeQuery = true)
	List<Produto> findByNameDescriptionMinMaxPrice(@Param("name_desc") String name_desc, @Param("min_price") double min_price,
			@Param("max_price") double max_price);
}
