package com.fb.springrest.api.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProdutoNotFoundException extends RuntimeException {
	public ProdutoNotFoundException(String id) {
		super("Produto Não Encontrado, ID: " + id);
	}
}
